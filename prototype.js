function Person(name){
   this.name = name;
}
Person.prototype = {
  sayHello: function(){ return 'Hi, i am '+this.name },
  company: {name:'ACME'}
}

function Employee(name,salary){
	Person.apply(this, arguments)
    this.salary = salary;
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.doWork = function(){ return ' i want my '+this.salary; }

var alice = new Employee('alice',1400);