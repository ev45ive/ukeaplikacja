
function Koszyk() {
        
    this.produkty = [],
    
    this.suma = 0,
    
    this.ustawIlosc = function (produktIndex, ilosc) {
        this.produkty[produktIndex].amount = ilosc;
        this.aktualizuj()
    }
    
    this.dodajProdukt = function (nazwa, ilosc, cena, podatek) {
        if (podatek === undefined) {
            podatek = 23
        }
        this.produkty.push({
            name: nazwa, amount: ilosc, price: cena, tax: podatek, podsuma: 0
        })
        this.aktualizuj()
    }
    
    this.wyswietl = function () {
        console.log('Suma produktów: ' + this.suma)
    }
    
    this.aktualizuj = function () {
        this.suma = 0;
        var self = this;
        this.produkty.forEach(function (product) {
            product.podsuma = self.calculate(product.amount, product.price, product.tax);
            self.suma += product.podsuma
        })
    }
    
    this.calculate = function (amount, price, tax, promotion) {
        return amount * ((1 + tax / 100) * price)
    }
}


var koszyk = new Koszyk()

koszyk.dodajProdukt('Produkt B',2,100,23)
koszyk.dodajProdukt('Produkt A',1,150,23)
koszyk.dodajProdukt('Placki',10,20);

koszyk.ustawIlosc(0,3)
koszyk.wyswietl()
