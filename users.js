
function makeRow(user){
    return $('<tr>\
        <th>'+ user.id +'</th>\
        <td>'+ user.name + '</td>\
        <td>'+ user.username + '</td>\
        <td>\
            <div class="btn-group btn-group-xs">\
                <button class="btn btn-default" data-action="edit" data-id="'+ user.id +'">Edit</button>\
                <button class="btn btn-default" data-action="remove" data-id="'+ user.id +'">Remove</button>\
            </div>\
        </td>\
    </tr>')
}


var users = [{
    id:1, name: 'alice', username:'alisson'
}]


var tbody = $('.users_table tbody')

// RENDER TABLE
function renderuj(users){
    tbody.empty()
    users.forEach(function(user){    
        tbody.append( makeRow(user) )
    })
}

function pobierzDane(){
    $.getJSON('http://localhost:3000/users/').then( function(data){ 
        renderuj(data)
    })
}
pobierzDane()


var form = $('.user_form')

tbody.on('click', '[data-action="remove"]',function(){
    
    $.ajax({
        url: 'http://localhost:3000/users/'+$(this).data('id'),
        type: 'DELETE',
    }).then(function(){
        pobierzDane()
    })
})

// EVENTS:
form.on('submit', function(event){
		
    // CREATE USER FROM FORM FIELDS
	var user = {
        //id: users.length+1,
        name: form.find('[name=name]').val(),
	    username: form.find('[name=surname]').val(),
    }
    // ADD TABLE ROW:
     tbody.append( makeRow(user) )

     // SEND TO SERVER:
     $.post('http://localhost:3000/users/', user).then(function(){ 
        pobierzDane()
     })

	event.preventDefault()
});






